#
##    Copyright (c) 2013, George Danezis and Rebecca Murphy
##     All rights reserved.
##
##    Redistribution and use in source and binary forms, with or without modification, 
##    are permitted provided that the following conditions are met:
##    * Redistributions of source code must retain the above copyright notice, this 
##    list of conditions and the following disclaimer.
##    * Redistributions in binary form must reproduce the above copyright notice, this 
##    list of conditions and the following disclaimer in the documentation and/or 
##    other materials provided with the distribution.
##
##    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
##    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
##    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
##    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
##    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
##    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
##    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
##    ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
##    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
##    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##

import random

from bezier import bboxoverlap

def seg2bb(seg):
    _,_,x0,y0,_,x1,y1 = seg
    xmin, xmax = min(x0,x1), max(x0,x1)
    ymin, ymax = min(y0,y1), max(y0,y1)
    return ((xmin, xmax), (ymin, ymax))


class AreaTree:
    def __init__(self):
        self.branches = {} 
        self.leaves = {}
        self.segments = None
        self.bboxes = []
        self.treebbox = None
        self.__position = 0

    def build_tree(self, segments):
        self.segments = segments
        
        pxmin, pxmax, pymin, pymax = None, None, None, None

        ## Pre-process the segments
        for _,_, x0, y0, _, x1, y1 in segments:
            xmin, xmax = min(x0,x1), max(x0,x1)
            ymin, ymax = min(y0,y1), max(y0,y1)
            self.bboxes += [((xmin, xmax), (ymin, ymax))]
            if pxmin == None:
                pxmin, pxmax, pymin, pymax = xmin, xmax, ymin, ymax
            else:
                pxmin, pxmax, pymin, pymax = min(pxmin,xmin), max(pxmax,xmax), min(pymin, ymin), max(pymax, ymax)        

        self.treebbox = ((pxmin, pxmax), (pymin, pymax))
        
        def __build_one_level(level, bbox, segs):            
            rsegs = random.choice(segs)
            Ave = self.segments[rsegs][2+(level % 2)]
            (xmin, xmax), (ymin, ymax) = bbox
            if (level % 2) == 0:
                B1 = ((xmin, Ave), (ymin, ymax))
                B2 = ((Ave, xmax), (ymin, ymax))
            else:
                B1 = ((xmin, xmax), (ymin, Ave))
                B2 = ((xmin, xmax), (Ave, ymax))

            L1, L2 = [], []
            for s in segs:
                if bboxoverlap(self.bboxes[s], B1):
                    L1 += [s]
                if bboxoverlap(self.bboxes[s], B2):
                    L2 += [s]
            return B1, L1, B2, L2

        ## Provides us with unique IDs
        
        def next_pos():
            p = self.__position
            self.__position += 1
            return p

        ## Build the tree
        current_bbox = ((pxmin, pxmax), (pymin, pymax))
        working_set = [(next_pos(), 0, current_bbox, range(len(self.segments)))]
        while not working_set == []:
            position, level, bbox, segs = working_set.pop(0)
            if segs == []:
                continue
            if level > 8:
                self.leaves[position] = segs
                continue
            
            B1, L1, B2, L2 = __build_one_level(level, bbox, segs)

            p1 = next_pos()
            p2 = next_pos()
            self.branches[position] = (B1, p1, B2, p2)            

            working_set += [(p1, level+1, B1, L1), (p2, level+1, B2, L2)]
                       

    def get_segments(self, segment):

        segbbox = seg2bb(segment)
        if not bboxoverlap(self.treebbox, segbbox):
            return frozenset([])
        
        matched_segments = [0]
        candidates = []
        while not matched_segments == []:
            j = matched_segments.pop(0)
            if j in self.branches:
                B1, p1, B2, p2 = self.branches[j]
                if bboxoverlap(B1, segbbox):
                    matched_segments += [p1]
                if bboxoverlap(B2, segbbox):
                    matched_segments += [p2]
            elif j in self.leaves:
                ## We are at a leaf
                candidates += self.leaves[j]
            else:
                pass
        
        candidate_set = set(candidates)
        new_candidate_set = []
        for c in candidate_set:            
            cbbox = self.bboxes[c]
            if bboxoverlap(cbbox, segbbox):
                new_candidate_set += [self.segments[c]]

        return frozenset(new_candidate_set)
            
    def debug_picture(self):
        xs, ys = [], []
        for _,_,x0,y0,_,x1,y1 in self.segments:
            xs += [np.real(x0), np.real(x1)]
            ys += [np.real(y0), np.real(y1)]
        plt.scatter(xs,ys, s=40, color="red")

        for B1, p1, B2, p2 in self.branches.values():
            for B,p in [(B1,p1), (B2,p2)]:
                if p in self.branches:
                    continue
                B = (B[0][0], B[0][1], B[1][0], B[1][1])
                plt.hlines([B[2], B[3]], B[0], B[1])
                plt.vlines([B[0], B[1]], B[2], B[3])

        plt.show()
        plt.cla()
