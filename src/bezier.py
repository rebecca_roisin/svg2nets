##
##    Copyright (c) 2013, George Danezis and Rebecca Murphy
##     All rights reserved.
##
##    Redistribution and use in source and binary forms, with or without modification, 
##    are permitted provided that the following conditions are met:
##    * Redistributions of source code must retain the above copyright notice, this 
##    list of conditions and the following disclaimer.
##    * Redistributions in binary form must reproduce the above copyright notice, this 
##    list of conditions and the following disclaimer in the documentation and/or 
##    other materials provided with the distribution.
##
##    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
##    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
##    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
##    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
##    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
##    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
##    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
##    ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
##    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
##    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##

import numpy as np
import random

## The fancy way of evaluating a spliting a cubic spline 
def dcast(curve, t):
    (p0, p1, p2, p3) = curve
    p01 = (1-t) * p0 + t * p1
    p12 = (1-t) * p1 + t * p2
    p23 = (1-t) * p2 + t * p3

    p012 = (1-t) * p01 + t * p12
    p123 = (1-t) * p12 + t * p23

    p0123 = (1-t) * p012 + t * p123
    return p0123, (p0, p01, p012, p0123), (p0123, p123, p23, p3)

## The naive way of evaluating a cubic spline
def naive(curve, t):
    (p0, p1, p2, p3) = curve
    return ((1-t)**3)*p0 + 3*((1-t)**2)*t*p1 +3*(1-t)*(t**2)*p2 + (t**3)*p3

## Evaluate at many points a cubic spline
def coords(curve, ts):
    x,y = [],[]
    for t in ts:
        xy = naive(curve, t)
        x += [xy[0]]
        y += [xy[1]]
    return x,y

## Get extreme points and inflection points
def extremes(curve):
    (p0, p1, p2, p3) = curve
    w0 = 3*(p1-p0)
    w1 = 3*(p2-p1)
    w2 = 3*(p3-p2)
    a = w0-2*w1+w2
    b = -2*w0 + 2*w1
    c = w0
    r1 = np.roots([a[0],b[0],c[0]])
    r2 = np.roots([a[1],b[1],c[1]])
    r = np.hstack((r1, r2))
    r = r[(0.0 < r) & (r < 1.0)]
    R = [0.0] + sorted([ri for ri in r if not np.iscomplex(ri)]) + [1.0]
    return R

## Determine the bounding box
def bbox(curve):
    candidates = extremes(curve)
    x,y = [], []
    for t in candidates:
        xy = naive(curve, t)
        x += [xy[0]]
        y += [xy[1]]
    
    xs = (min(x), max(x))
    ys = (min(y), max(y))
    return xs, ys, candidates

## Do two bounding boxes overlap?
def bboxoverlap(bbox1, bbox2):
    (xmin1,xmax1), (ymin1,ymax1) = bbox1
    (xmin2,xmax2), (ymin2,ymax2) = bbox2

    if xmax1 <= xmin2 or  xmax2 <= xmin1 or \
       ymax1 <= ymin2 or  ymax2 <= ymin1:
        return False
    else:
        return True

def pointinbbox(bbox, x, y):
    (xmin,xmax), (ymin,ymax) = bbox
    return xmin <= x <= xmax and \
           ymin <= y <= ymax

