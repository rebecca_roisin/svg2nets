import findpaths
import numpy as np
import matplotlib.pyplot as plt

from curvebag import CurveBag
from bezier import naive

import sys
import os.path

def transitive_closure(a):
    closure = set(a)
    while True:
        new_relations = set((x,w) for x,y in closure for q,w in closure if q == y)
        closure_until_now = closure | new_relations
        if closure_until_now == closure:
            break
        closure = closure_until_now

    return closure

def relations_to_equivalences(labels):
    relations = set()
    all_paths = set()
    for (p1, p2) in labels:
        all_paths.add(p1)
        all_paths.add(p2)
        relations.add((p1, p2))
        relations.add((p2, p1))

    all_relations = transitive_closure(relations)
    path_lists = {}
    for (p1,p2) in all_relations:
        if p1 not in path_lists:
            path_lists[p1] = set()
        path_lists[p1].add(p2)

    all_sets = set([frozenset(s) for s in path_lists.values()])
    return all_sets

def main(filename):
    if not os.path.exists(filename):
        print 'Cannot find file "%s"' % filename
        sys.exit()
    
    # A curve bag is the main interface to get intersections
    # and detect whether points are inside shapes.
    cb = CurveBag()

    # Add all Bezier curve descriptors into the bag
    # annotated with their path ID.
    all_curves, _ = findpaths.open_svg(filename)
    for path_id in all_curves:
        for b in all_curves[path_id]:
            cb.add_curve(b, path_id)

    # Find connection points between path IDs
    # Set param to True to find *all* intersections
    # otherwise it only looks for the first one.
    # (all not needed just for extracting a netlist)
    matches, labels = cb.find_intersections(True)

    ## Make sets of paths that are connected
    classes = relations_to_equivalences(labels)

    ## Print the sets
    for i, s in enumerate(classes):
        print "%s: %s" % (i,",".join(s))
    
    # get curves from quartets
    plots = []
    t = np.arange(0.0, 1.0, 0.1)
    xs, ys = [], []
    for bezier in all_curves.values():    
        for j in bezier:
            #cvs = []
            for k in t:
                pt = naive(j,k)
                xs.append(pt[0])
                ys.append(-pt[1]) 

    xsr, ysr = [],[]
    for cid, _, t, _ in matches:
        pt = naive(cb.curves[cid],t)
        xsr.append(np.real(pt[0]))
        ysr.append(np.real(-pt[1])) 
        
    if len(xs) > 0:
        try:
            plt.scatter(xs, ys, s=1)
            plt.scatter(xsr, ysr, color="red", s=50)
            print "... writing output.png."
            plt.savefig("output.png")
        finally:
            plt.cla()
            plt.close()

    return 

import profile
if __name__ == "__main__":
    try:
        main(sys.argv[1])
    except:
        print "usage: python svg2nets.py filename"
