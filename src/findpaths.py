##
##    Copyright (c) 2013, George Danezis and Rebecca Murphy
##     All rights reserved.
##
##    Redistribution and use in source and binary forms, with or without modification, 
##    are permitted provided that the following conditions are met:
##    * Redistributions of source code must retain the above copyright notice, this 
##    list of conditions and the following disclaimer.
##    * Redistributions in binary form must reproduce the above copyright notice, this 
##    list of conditions and the following disclaimer in the documentation and/or 
##    other materials provided with the distribution.
##
##    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
##    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
##    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
##    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
##    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
##    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
##    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
##    ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
##    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
##    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##

import re
import numpy as np
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
import svg.path as svgp 


def open_svg(f):
    ## need to make this into dictionary!!
    tree = ET.parse(f)
    root = tree.getroot()
    curves = []
    circles = []
    for r in root.iter("{http://www.w3.org/2000/svg}path"):
        curves.append((r.attrib['d'], r.attrib['id']))
    for c in root.iter("{http://www.w3.org/2000/svg}circle"):
        circles.append((float(c.attrib['cx']), -float(c.attrib['cy']), c.attrib['id']))
    return parse_svg_fancy(curves), circles


#def parse_svg(name = "bezier3.svg"):
#    path, _ = svg_parser(name)  
#    
#    # get bezier curve quartets  
#    all_curves = {}
#    for i in path:
#        bezier = make_absolute(i[0])
#        all_curves[i[1]] = bezier
#
#    return all_curves

def cplx_to_array(num):
    return np.array([num.real, num.imag])


def  parse_svg_fancy(svg_list):
    parsed_paths = {}
    # use svg.path to read curves appropriately as quartets
    for i in svg_list:
        try:
            parsed_paths[i[1]] = svgp.parse_path(i[0])
            p = parsed_paths[i[1]][2]
        except:
            print i[0]
            raise

    # go through paths and add path elements to dictionary as quartets of values (start, c1, c2, end)
    quad_paths = {}
    for j in parsed_paths.keys():
        quadruplet = []
        for k in parsed_paths[j]:
            if type(k) == svgp.CubicBezier:
                quadruplet.append((cplx_to_array(k.start), cplx_to_array(k.control1), cplx_to_array(k.control2), cplx_to_array(k.end)))
                quad_paths[j] = quadruplet
            elif type(k) == svgp.Line:
                quadruplet.append((cplx_to_array(k.start), cplx_to_array(k.start), cplx_to_array(k.end), cplx_to_array(k.end)))
                quad_paths[j] = quadruplet
            else:
                
                raise Exception("What was that?")
    
    #print quad_paths.values()    
    return quad_paths

 
# function to convert quartet of values into bezier curve
def naive(curve, t):
    (p0, p1, p2, p3) = curve
    return ((1-t)**3)*p0 + 3*((1-t)**2)*t*p1 +3*(1-t)*(t**2)*p2 + (t**3)*p3
 

if __name__ == "__main__":

    curves, circles = open_svg("../examples/circuit1.svg")    
    parsed_curves = parse_svg_fancy(curves)

    # get curves from quartets
    plots = []
    t = np.arange(0.0, 1.0, 0.1)
    xs, ys = [], []
    for bezier in parsed_curves.values():    
        for j in bezier:
            #cvs = []
            for k in t:
                pt = naive(j,k)
                xs.append(pt[0])
                ys.append(-pt[1]) 
    xc, yc = [], []
    for i, j, _ in circles:
        xc.append(i)
        yc.append(j)
   

    if len(xs) > 0:
        plt.scatter(xs, ys, s=1)
    if len(xc) > 0: 
        plt.scatter(xc, yc, s=50, c="r")

    plt.show()
    plt.cla()

