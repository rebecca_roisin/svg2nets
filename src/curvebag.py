##
##    Copyright (c) 2013, George Danezis and Rebecca Murphy
##     All rights reserved.
##
##    Redistribution and use in source and binary forms, with or without modification, 
##    are permitted provided that the following conditions are met:
##    * Redistributions of source code must retain the above copyright notice, this 
##    list of conditions and the following disclaimer.
##    * Redistributions in binary form must reproduce the above copyright notice, this 
##    list of conditions and the following disclaimer in the documentation and/or 
##    other materials provided with the distribution.
##
##    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
##    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
##    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
##    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
##    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
##    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
##    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
##    ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
##    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
##    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##

import numpy as np

from bezier import naive, bbox, bboxoverlap, coords, pointinbbox
from areatree import AreaTree


class CurveBag:
    def __init__(self, T = 0.001):
        self.T = T
        self.curves = {}
        self.paths = {}
        self.paths_sets = {}
        self.paths_info = {}
        self.paths_trees = {}
        
        self.curve_data = {}
        self.next_id = 0

        self.segments = []
        self.bbox_cache = {}

    def get_next_id(self):
        self.next_id += 1
        return self.next_id

    def add_curve(self, curve, path_id, curve_id = None):
        if curve_id == None:
            curve_id = self.get_next_id()

        self.curves[curve_id] = curve
        self.curve_data[curve_id] = bbox(curve)
        self.paths[curve_id] = path_id

        if path_id not in self.paths_sets:
           self.paths_sets[path_id] = []
           self.paths_info[path_id] = None

        ts = self.curve_data[curve_id][2]
        xs, ys = coords(self.curves[curve_id], ts)
        self.paths_sets[path_id] += [(curve_id, ts[i], xs[i], ys[i], ts[i+1], xs[i+1], ys[i+1]) for i,_ in enumerate(ts[:-1])]                

        if self.paths_info[path_id] == None:
            self.paths_info[path_id] = ((min(xs), max(xs)), (min(ys), max(ys)))
        else:
            ((oxmin, oxmax), (oymin, oymax)) = self.paths_info[path_id]
            self.paths_info[path_id] = ((min(xs+[oxmin]), max(xs+[oxmax])), (min(ys+[oymin]), max(ys+[oymax])))

        # self.paths_sets[path_id] += []

        return curve_id

    def __xy3bb(self, seg1, seg2):
        T=self.T
        try:
            cida, t0a, x0a, y0a, t1a, x1a, y1a = seg1
            cidb, t0b, x0b, y0b, t1b, x1b, y1b = seg2
        except:
            print seg1, seg2
            
        if cida == cidb:
            return False, []

        ## Python's method call is too slow -- so I am inlining those
        if seg1 not in self.bbox_cache:            
            xA,yA = (x0a, x1a),(y0a, y1a)
            bbA = ((min(xA), max(xA)), (min(yA), max(yA)))
            self.bbox_cache[seg1] = bbA
        else:
            bbA = self.bbox_cache[seg1]

        if seg2 not in self.bbox_cache:           
            xB,yB = (x0b, x1b),(y0b, y1b)
            bbB = ((min(xB), max(xB)), (min(yB), max(yB)))
            self.bbox_cache[seg2] = bbB
        else:
            bbB = self.bbox_cache[seg2]
            
        if bboxoverlap(bbA, bbB):
            if ((bbA[0][1] - bbA[0][0]) <= T and (bbA[1][1] - bbA[1][0]) <= T) and ((bbB[0][1] - bbB[0][0]) <= T and (bbB[1][1] - bbB[1][0]) <= T):                    
                return True, [(cida, cidb, round(np.real((t0a+t1a)) / 2.0, 3), round(np.real((t0b+t1b))/2.0, 3))]

            xya = naive(self.curves[cida], (t0a+t1a)/2.0)
            xyb = naive(self.curves[cidb], (t0b+t1b)/2.0)
            
            return False, [([(cida, t0a, x0a, y0a, (t0a+t1a)/2.0, xya[0], xya[1]),
                    (cida, (t0a+t1a)/2.0, xya[0], xya[1], t1a, x1a, y1a)],
                    [(cidb, t0b, x0b, y0b, (t0b+t1b)/2.0, xyb[0], xyb[1]),
                    (cidb, (t0b+t1b)/2.0, xyb[0], xyb[1], t1b, x1b, y1b)])]
        else:                
            return False, []

    def find_point(self, x, y, findany=True):
        ## Make a line
        T = self.T
        massive_seg = (None, None, x,y, None, x,y+10000)
        bbox = ((x,x), (y, y+10000))
        insides = set()

        ## Determine if we are inside any path, one by one
        for pset1 in self.paths_sets:
            if not pointinbbox(self.paths_info[pset1], x, y):
                continue
            if pset1 not in self.paths_trees: 
                tree = AreaTree()
                tree.build_tree(self.paths_sets[pset1])
                self.paths_trees[pset1] = tree
            else:
                tree = self.paths_trees[pset1]

            segs = tree.get_segments(massive_seg)

            ## Find the actual intersection
            candidates = list(segs)
            intersects = set()
            while candidates != []:
                seg1 = candidates.pop(0)
                cida, t0a, x0a, y0a, t1a, x1a, y1a = seg1
                xA,yA = (x0a, x1a),(y0a, y1a)
                bbA = ((min(xA), max(xA)), (min(yA), max(yA)))

                if not bboxoverlap(bbA, bbox):
                    continue

                xya = naive(self.curves[cida], (t0a+t1a)/2.0)
                if ((bbA[0][1] - bbA[0][0]) <= T and (bbA[1][1] - bbA[1][0]) <= T):
                    intersects.add((cida, round((t0a+t1a) / 2.0, 3)))
                else:
                    candidates += [(cida, t0a, x0a, y0a, (t0a+t1a)/2.0, xya[0], xya[1]),
                                   (cida, (t0a+t1a)/2.0, xya[0], xya[1], t1a, x1a, y1a)]

            ## The ray-casting algorithm: odd = inside
            if len(intersects) % 2 == 1:
                insides.add(pset1)
                if findany:
                    return insides
        return insides

    def find_intersections(self, findany=True):
        matches = set()
        ignore_seg = set()
        
        families = []
        for pset1 in self.paths_sets:
            ## Bluid a tree if we have to
            if pset1 not in self.paths_trees: 
                tree = AreaTree()
                tree.build_tree(self.paths_sets[pset1])
                self.paths_trees[pset1] = tree
            else:
                tree = self.paths_trees[pset1]
            
            for pset2 in self.paths_sets:
                if pset1 >= pset2 or not bboxoverlap(self.paths_info[pset1], self.paths_info[pset2]):
                    continue

                local_families = []
                for s in self.paths_sets[pset2]:
                    fs = list(tree.get_segments(s))
                    if fs == frozenset():
                        continue
                    local_families += [(fs, [s])]

                if local_families != []:
                    families += [((pset1, pset2), local_families)]

        local_family = []
        label = None
        matched_labels = set()
        while local_family != [] or families != []:

            if local_family == []:
                label, local_family = families.pop(0)

            this_family1, this_family2 = local_family.pop(0)
            
            B=False
            for seg1 in this_family1:
                for seg2 in this_family2:
                    
                    ret, new_fam = self.__xy3bb(seg1, seg2)
                    if ret == False:
                        local_family = new_fam + local_family
                        
                    else:
                        matches.add(new_fam[0])
                        matched_labels.add(label)
                        if findany:
                            local_family = []
                            B = True
                        
                        break
                if B:
                    break

        print "Number of matches", len(matches)
        return matches, matched_labels

